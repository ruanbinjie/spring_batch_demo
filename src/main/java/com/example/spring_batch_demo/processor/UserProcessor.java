package com.example.spring_batch_demo.processor;

import com.example.spring_batch_demo.entity.User;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class UserProcessor implements ItemProcessor<User,String> {

    @Override
    public String process(User user) throws Exception {
        System.out.println("process======="+user.getName());
        return user.getName();
    }
}
