package com.example.spring_batch_demo.factory;

import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.batch.MyBatisPagingItemReader;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.skip.AlwaysSkipItemSkipPolicy;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.retry.policy.NeverRetryPolicy;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Slf4j
public class BatchStepFactory {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    @Qualifier("mySqlFactory")
    public MybatisSqlSessionFactoryBean hermesMybatisSqlSessionFactoryBean;

    public <S, I> Step createDefaultStepWithNameAndSize(ItemReader<S> itemReader, ItemProcessor<S, I> itemProcessor
            , ItemWriter<I> itemWriter, String stepName, Integer size) {
        return stepBuilderFactory.get(stepName)
                .<S, I>chunk(size).reader(itemReader).faultTolerant().retryPolicy(new NeverRetryPolicy()).
                        skipPolicy(new AlwaysSkipItemSkipPolicy())
                .processor(itemProcessor).faultTolerant().retryPolicy(new NeverRetryPolicy()).
                        skipPolicy(new AlwaysSkipItemSkipPolicy())
                .writer(itemWriter).faultTolerant().retryPolicy(new NeverRetryPolicy()).
                        skipPolicy(new AlwaysSkipItemSkipPolicy()).build();
    }

    @SneakyThrows
    public <S> MyBatisPagingItemReader<S> createMybatisReader(String queryId, Map<String
            , Object> parameterValues, Integer pageSize) {
        MyBatisPagingItemReader<S> reader = new MyBatisPagingItemReader<>();
        reader.setQueryId(queryId);
        reader.setParameterValues(parameterValues);
        reader.setPageSize(pageSize);
        reader.setSqlSessionFactory(hermesMybatisSqlSessionFactoryBean.getObject());
        reader.afterPropertiesSet();
        return reader;
    }
}
