package com.example.spring_batch_demo.config;

import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.omg.PortableInterceptor.Interceptor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;

@Configuration
@MapperScan(basePackages = "com.example.spring_batch_demo", sqlSessionTemplateRef = "aidedSqlSessionTemplate")
public class DataSouceConfig {

    @Bean("myDB")
    @ConfigurationProperties("spring.datasource.self")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean("mySqlFactory")
    public MybatisSqlSessionFactoryBean sqlSessionFactory(@Qualifier("myDB") DataSource dataSource) throws IOException, SQLException {
        boolean valid = dataSource.getConnection().isValid(10);
        System.out.println("数据库链接状态:"+valid);
        MybatisSqlSessionFactoryBean sqlSessionFactoryBean = new MybatisSqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:/mapping/*Mapper.xml"));
        MybatisConfiguration configuration = new MybatisConfiguration();
        configuration.setDefaultExecutorType(ExecutorType.BATCH);
        sqlSessionFactoryBean.setConfiguration(configuration);
        return sqlSessionFactoryBean;
    }

    @Bean(name = "aidedTransactionManager")
    @Primary
    public DataSourceTransactionManager mysqlTransactionManager(@Qualifier("myDB") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "aidedSqlSessionTemplate")
    @Primary
    public SqlSessionTemplate mysqlSqlSessionTemplate(@Qualifier("mySqlFactory") SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }
}
