package com.example.spring_batch_demo.config;

import com.example.spring_batch_demo.entity.User;
import com.example.spring_batch_demo.factory.BatchStepFactory;
import com.example.spring_batch_demo.processor.UserProcessor;
import com.example.spring_batch_demo.reader.UserReader;
import com.example.spring_batch_demo.writer.UserWriter;
import org.springframework.batch.core.Step;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StepConfig {

    @Autowired
    private BatchStepFactory batchStepFactory;

    @Autowired
    private UserReader userReader;

    @Autowired
    private UserProcessor userProcessor;

    @Autowired
    private UserWriter userWriter;

    public Step userStep() {
        return batchStepFactory.<User, String>createDefaultStepWithNameAndSize
                (userReader.<User>createMybatisReader(), userProcessor, userWriter, "userStep", 1);
    }
}
