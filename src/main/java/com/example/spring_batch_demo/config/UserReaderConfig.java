package com.example.spring_batch_demo.config;

import com.example.spring_batch_demo.entity.User;
import com.example.spring_batch_demo.factory.BatchStepFactory;
import org.mybatis.spring.batch.MyBatisPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

@Configuration
public class UserReaderConfig {

    @Autowired
    private BatchStepFactory batchStepFactory;

    public MyBatisPagingItemReader<User> selectAllUserReader() {
        return batchStepFactory.createMybatisReader("com.example.spring_batch_demo.mapper.selectAllUsers",
                new HashMap<>(), 1);
    }
}
