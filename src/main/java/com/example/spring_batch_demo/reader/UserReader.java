package com.example.spring_batch_demo.reader;

import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import com.example.spring_batch_demo.entity.User;
import lombok.SneakyThrows;
import org.mybatis.spring.batch.MyBatisPagingItemReader;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class UserReader {

    private final String queryId = "com.example.spring_batch_demo.mapper.selectAllUsers";
    private final Map<String, Object> parameterValues = new HashMap<>();
    private final int pageSize = 1;

    @Autowired
    @Qualifier("mySqlFactory")
    public MybatisSqlSessionFactoryBean hermesMybatisSqlSessionFactoryBean;

    @SneakyThrows
    public <S> MyBatisPagingItemReader<S> createMybatisReader() {
        MyBatisPagingItemReader<S> mybatisReader = this.createMybatisReader(queryId, parameterValues, pageSize);
        return mybatisReader;
    }

    @SneakyThrows
    public <S> MyBatisPagingItemReader<S> createMybatisReader(String queryId, Map<String
            , Object> parameterValues, Integer pageSize) {
        MyBatisPagingItemReader<S> reader = new MyBatisPagingItemReader<>();
        reader.setQueryId(queryId);
        reader.setParameterValues(parameterValues);
        reader.setPageSize(pageSize);
        reader.setSqlSessionFactory(hermesMybatisSqlSessionFactoryBean.getObject());
        reader.afterPropertiesSet();
        int page = reader.getPage();
        int pageSize1 = reader.getPageSize();
        System.out.println("page" + page);
        System.out.println("pageSize1" + pageSize1);
        return reader;
    }
}
