package com.example.spring_batch_demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.spring_batch_demo.entity.User;

import java.util.List;

public interface UserMapper extends BaseMapper<User> {

    List<User> selectAllUsers();
}
