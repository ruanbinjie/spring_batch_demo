package com.example.spring_batch_demo.entity;

import lombok.Data;

import javax.persistence.Table;

@Table(name = "tb_user")
@Data
public class User {

    private Integer id;

    private String name;

    private Integer age;
}
