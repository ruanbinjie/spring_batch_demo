package com.example.spring_batch_demo.entity;

import lombok.Data;

import java.util.Date;

@Data
public class CurrencyDO {

    private Long crId;
    private String mdmCode;
    private String code;
    private String name;
    private String countryCode;
    private String language;
    private String source;
    private Integer level;
    private Integer status;
    private Date startTime;
    private Date endTime;
    private String remark;
    private Integer hasFlag;
    private Integer isEnable;
    private Integer crPrecision;
    private Integer extendPrecision;
}
