package com.example.spring_batch_demo.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tb_message")
@Data
public class Message {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "content")
    private String content;
}
