package com.example.spring_batch_demo.writer;

import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Component
public class UserWriter implements ItemWriter<String> {
    @Override
    public void write(List<? extends String> list) throws Exception {
        OutputStream outputStream = new FileOutputStream("/Users/admin/user.txt");
        for (String str : list) {
            System.out.println(str);
            outputStream.write(str.getBytes(StandardCharsets.UTF_8));
        }
        outputStream.close();
    }
}
