package com.example.spring_batch_demo.component;

import org.springframework.batch.item.ItemProcessor;

import java.util.Locale;

public class Processor implements ItemProcessor<String, String> {
    @Override
    public String process(String s) throws Exception {
        return s.toUpperCase(Locale.ROOT);
    }
}
