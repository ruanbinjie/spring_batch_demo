package com.example.spring_batch_demo.component;


import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class Writer implements ItemWriter<String> {
    @Override
    public void write(List<? extends String> list) throws Exception {
        for (String msg : list) {
            System.out.println("data为" + msg);
        }
    }
}
